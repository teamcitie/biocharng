<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Softon\SweetAlert\Facades\SWAL;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function getSignup ()
    {
    	return view('pages.signup');
    }

    public function postSignup (Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required|unique:users|email|max:255',
    		'password' => 'required|confirmed|min:6',
    		// 'password_confirmation' => 'required|same:password'
    		'first_name' => 'required|max:255',
    		'last_name' => 'required|max:255',
    		'institution' => 'required|max:255',
    		'phone_number' => 'required',
    	]);

    	User::create([
    		'institution' => $request->input('institution'),
    		'phone_number' => $request->input('phone_number'),
    		'member_number' => $request->input('member_number'),
    		'email' => $request->input('email'),
    		'password' => bcrypt($request->input('password')),
    		'first_name' => $request->input('first_name'),
    		'last_name' => $request->input('last_name'),
    	]);

        $message = swal()->position('top-right')->toast()->autoclose(9000)->message('Thank you','Your account has been created and you can now sign in!','success');
    	return redirect()
    			->route('auth.signin')
    			->with('message', $message);
    }

    public function getSignin ()
    {
    	return view('pages.signin');
    }

    public function postSignin (Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required',
    		'password' => 'required',
    	]);

    	if (!Auth::attempt($request->only(['email', 'password']), $request->has('remember'))) {
            // SWAL::message('Sorry','You cannot log in with these details!','error');
            swal()->position('top-right')->toast()->button('Try again')->message('Sorry','You cannot log in with these details!','info');
    		return redirect()
    			// ->swal()->toast()->autoclose(2000)->message('Good Job','You have successfully logged In!','info')
                ->back();
    	}

        $message = swal()->position('top-right')->toast()->autoclose(9000)->message('Good Job','You have successfully logged In!','info');
        
    	return redirect()
    		->route('home')
    		->with('message', $message);
    }

    public function getSignout()
    {
    	Auth::logout();
    	return redirect()->route('home');
    }

}
