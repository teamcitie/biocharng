<?php

namespace App\Http\Controllers;

use App\Models\Paper;
use Cloudder;
use Illuminate\Http\Request;

class PaperController extends Controller
{
    public function getSubmit ()
    {
    	return view('pages.submitpaper');
    }

    public function postSubmit (Request $request)
    {
    	//Validate the submitted request
    	$this->validate($request, [
    		'fullname' 		=> 'required',
    		'email' 		=> 'required',
    		'phone_number' 	=> 'required',
    		'paper' 		=> 'required|mimes:docx',
    		'section' 		=> 'required',
    	]);

    	//check if there is a file in the request submitted
    	if ( $request->hasFile('paper')) {

		    $file = $request->file('paper');

		    //Get the file extension
		    $ext = strtolower($file->getClientOriginalExtension());

		    //Generate a filename
		    $filename = $request->input('fullname'). "'s paper." . $ext;

		    /*
		    Upload the file to Cloudinary and $paperData takes
		    the value that will be inserted in the database
			*/
		    $paperData = Cloudder::upload($file, $filename, [
		    	"resource_type" => "raw",
		    ]);
		    $result = Cloudder::getResult();
		    $data = json_encode($result);
		}
		// dd($data);
		// using php function to remove the ".tmp" in front of the public url

		//Submit to the paper table in the database
		Paper::create([
			'fullname' 		=> $request->input('fullname'),
			'email' 		=> $request->input('email'),
			'phone_number' 	=> $request->input('phone_number'),
			'section' 		=> $request->input('section'),
			'file' 			=> $data,
		]);
		// dd($data);

    	$message = swal()->position('center')->toast()->autoclose(9000)->message('Thank you', 'Your paper has been submitted!', 'success');
    	return redirect()
    		->back()
    		->with('message', $message);
    }
}
