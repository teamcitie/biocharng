<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Softon\SweetAlert\Facades\SWAL;
use App\Models\User;

use Illuminate\Http\Request;


class PasswordResetController extends Controller
{
    private function codeGen($userId)
    {
        $dUser = User::find($userId);
        $val = sha1(uniqid());
        $dUser->reset = $val;
        $dUser->save();
        return $dUser->reset;
    }

    public function index ()
    {
    	return view('auth.passwords.email');
    }

    // sending reset email
    public function sendReset (Request $request)
    {
    	$user = User::whereEmail($request->email)->first();

    	if (!count($user)) {
    		$message = swal()->position('top-right')->toast()->autoclose(9000)->message('Sorry','You have typed an incorrect email!', 'error');
    		$alert = "You have typed an incorrect email";
    		return redirect()
    			->route('password.reset')
    			->with('message', $message)
    			->with('alert', $alert);
    	}

    	//2. grab users reminder or create one if it doesnt exist
        $userCode = $user->reset ?: $this->codeGen($user->id);

        $message = swal()->position('top-right')->toast()->autoclose(9000)->message('Thank you','A mail have been sent to your email address!', 'success');

        //3. send mail and display message
        $this->sendEmail($user, $userCode);
        return redirect()->back()->with('Message','A mail has been sent to your email address.');
    }

    private function sendEmail($user, $code)
    {
        Mail::send('auth.emails.password', [
            'user'=> $user,
            'code'=> $code
        ], function ($message) use ($user){
            $message->subject('Reset your Password');
            $message->from('no-reply@biocharng.com', 'Reset Password');
            $message->to($user->email);

            $message->subject("Reset your password");
        });
    }


    public function resetPassword($email, $code)
    {
        $user = User::where('email', $email)->first();

        if(count($user) == 0){
            abort(404);
        }else{
            if($user->reset == $code){
                return view('auth.passwords.reset');
            }else{
                abort(404);
            }
        }
    }

    public function postResetPassword(Request $request, $email, $code)
    {
        $this->validate($request, [
            'password' => 'confirmed|required|min:6'
        ]);

        $user = User::where('email', $email)->first();

        if(count($user) == 0){
            abort(404);
        }else{
            if($user->reset == $code){
                if($this->updateUserPassword($email, $request->input('password')));
                    $message = swal()->position('top-right')->toast()->autoclose(9000)->message('Thank you','You have successfully updated your password!', 'success');
                    return redirect('/');
            }else{
                abort(404);
            }
        }
    }

    private function updateUserPassword($email, $password)
    {
        $user = User::where('email', $email)->first();
        $user->password = bcrypt($password);
        $user->save();
        return true;
    }
}
