<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Paper;
use Softon\SweetAlert\Facades\SWAL;

class AdminController extends Controller
{

    public function index() {
    	$users = User::latest()->get();
    	$papers = Paper::latest()->get();

    	return view('dashboard.home')
    			->with('users', $users)
    			->with('papers', $papers);
    }

    public function getPapers () {
    	$papers = Paper::latest()->get();

    	return view('dashboard.papers')
				->with('papers', $papers);
    }

    public function getUsers () {
        $users = User::latest()->get();

        return view('dashboard.users')
                ->with('users', $users);
    }

}
