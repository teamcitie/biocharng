<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paper;
use Cloudder;

class DemoController extends Controller
{
    public function index () {
    	$paper = Paper::get()->first();
    	$p = unserialize($paper->file);
    	// $p = json_encode($p);
    	// $p = Cloudder::getResult();
    	$p = $p->uploadedResult;
    	dd($p);
    }
}
