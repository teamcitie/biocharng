<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
	use Notifiable;

    protected $table = 'papers';

    protected $fillable = [
        'fullname',
        'email',
        'phone_number',
        'file',
        'section',
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function getUserImg()
    {
        $url = json_decode($this->file, true);
        return $url['url'];
    }
}
