<header class="header header-shrink header-inverse fixed-top">
	<div class="container">
		<nav class="navbar navbar-expand-lg px-md-0">
			<a class="navbar-brand" href="/">
				<span class="logo-default">
					{{-- <img src="assets/img/logo-default.png" alt=""> --}}
					<img src="assets/img/bin-logo.png" alt="">
				</span>
				<span class="logo-inverse">
					{{-- <img src="assets/img/logo-inverse.png" alt=""> --}}
					<img src="assets/img/bin-logo.png" alt="">
				</span>
			</a>

			<button class="navbar-toggler p-0" data-toggle="collapse" data-target="#navbarNav">
	          <div class="hamburger hamburger--spin js-hamburger">
	                <div class="hamburger-box">
	                  <div class="hamburger-inner"></div>
	                </div>
	            </div>
			</button>

			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="/">HOME</a>
					</li>
					@if (!Auth::check())
						<li class="nav-item">
							<a class="nav-link" href="{{ route('auth.signup') }}">REGISTER</a>
						</li>
					@endif
					<li class="nav-item">
						<a class="nav-link" href="" data-scrollto="speakers">SPEAKERS</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="" data-scrollto="schedule">PROGRAMS</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">CON</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">CONTACT US</a>
					</li>
					@if (Auth::check())
						<li class="nav-item">
							<a class="nav-link" href="{{ route('auth.signout') }}">Signout</a>
						</li>
					@endif

					@if (Auth::check() && Auth::user()->admin == 1)
						<li class="nav-item">
							<a class="nav-link" href="{{ route('dashboard') }}">Dashboard</a>
						</li>
					@endif
				</ul>
			</div>

		</nav>
	</div> <!-- END container-->
</header> <!-- END header -->
