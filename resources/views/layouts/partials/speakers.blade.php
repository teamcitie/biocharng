<section id="sponsors" class="bg-gray-v1">
	 	<div class="container">
	 		<div class="row">
	 			<div class="col-12 text-center">
	 				<h2 class="h1">
	 					Event Sponsors
	 				</h2>
	 				<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-70 mx-auto"></div>
	 			</div>
	 		</div> <!-- END row-->
	 		<div class="row">
		 		{{-- <div class="col-12 text-center">
		 			<p class="d-inline-block u-fs-24 u-fw-600 bg-white text-red py-3 px-5 u-rounded-50">
		 				Platinum Sponsors
		 			</p>
		 		</div> --}}
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/1_1.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/1_2.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/1_3.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/1_4.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 		</div> <!-- END row-->
	 		<div class="row">
		 		{{-- <div class="col-12 text-center u-mt-80">
		 			<p class="d-inline-block u-fs-24 u-fw-600 bg-white text-primary py-3 px-5 u-rounded-50">
		 				Gold Sponsors
		 			</p>
		 		</div> --}}
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/2_1.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/2_2.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/2_3.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 			{{-- <div class="col-6 col-md-3 text-center mt-5">
	 				<div class="box-shadow-v2 bg-white u-flex-center u-h-100p p-4 p-md-5 rounded">
	 					<img src="/assets/img/event/sponsor/2_4.png" alt="">
	 				</div>
	 			</div> --}} <!-- END col-6 col-md-3-->
	 		</div> <!-- END row-->
	 	</div> <!-- END container-->
 	</section>