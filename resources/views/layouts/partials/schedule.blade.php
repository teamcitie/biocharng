<section id="schedule" data-parallax="/assets/img/event/bg-1.png">
	 	<div class="container">
	 		<div class="row">
	 			<div class="col-12 text-center">
	 				<h2 class="h1">
	 					Conference Schedule
	 				</h2>
	 				<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-70 mx-auto"></div>
	 			</div>
	 			<div class="col-12 box-shadow-v2 p-5">

					<ul class="nav nav-pills mb-3 justify-content-center u-ff-dosis u-fs-20 u-fw-600" role="tablist">
						<li class="nav-item">
							<a class="nav-link u-rounded-50 active" data-toggle="pill" href="#day1" role="tab" aria-selected="true" aria-expanded="true">Day - 1</a>
						</li>
						<li class="nav-item">
							<a class="nav-link u-rounded-50" data-toggle="pill" href="#day2" role="tab" aria-selected="false" aria-expanded="false">Day - 2</a>
						</li>
						<li class="nav-item">
							<a class="nav-link u-rounded-50" data-toggle="pill" href="#day3" role="tab" aria-selected="false" aria-expanded="false">Day - 3</a>
						</li>
						<li class="nav-item">
							<a class="nav-link u-rounded-50" data-toggle="pill" href="#day4" role="tab" aria-selected="false" aria-expanded="false">Day - 4</a>
						</li>
						<li class="nav-item">
							<a class="nav-link u-rounded-50" data-toggle="pill" href="#day5" role="tab" aria-selected="false" aria-expanded="false">Day - 5</a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade active show rounded" id="day1" role="tabpanel" aria-expanded="true">
							<p class="text-red u-my-40 text-center u-fw-600 u-fs-22">
								Monday, 10 September 2018
								<br>
								Venue: Post Graduate Auditorium
							</p>
				    		<ul class="list-unstyled u-fw-600">
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Arrival and Conference Registration
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Executive Committee Meeting
								</li>
							</ul>
						</div> <!-- END tab-pane-->
						<div class="tab-pane fade" id="day2" role="tabpanel" aria-expanded="true">
							<p class="text-red u-my-40 text-center u-fw-600 u-fs-22">
								Tuesday, 11 September 2018
								<br>
								Venue: Post Graduate Auditorium
							</p>
							<ul class="list-unstyled u-fw-600">
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Continuation of Registration
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									All participants and invited guests seated
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Opening Ceremony
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Photographs and Lunch break
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									1st Plenary Session
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									2nd Plenary Session
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Cocktail Dinner (Senate Building basement)
								</li>
							</ul>
						</div> <!-- END tab-pane-->
						<div class="tab-pane fade" id="day3" role="tabpanel" aria-expanded="true">
							<p class="text-red u-my-40 text-center u-fw-600 u-fs-22">
								Wednesday, 12 September 2018
							</p>
							<ul class="list-unstyled u-fw-600">
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Continuation of Registration
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Technical Session (Post-Graduate Auditorium)
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Tea / Cofee Break
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Technical Session (Post-Graduate Auditorium)
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Lunch Break
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Technical Session (Post-Graduate Auditorium)
								</li>
							</ul>
						</div> <!-- END tab-pane-->
						<div class="tab-pane fade" id="day4" role="tabpanel" aria-expanded="true">
							<p class="text-red u-my-40 text-center u-fw-600 u-fs-22">
								Thursday, 13 January 2018
							</p>
							<ul class="list-unstyled u-fw-600">
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Field Trip Lunch Break
								</li>
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Annual General Meeting
								</li>
							</ul>
						</div> <!-- END tab-pane-->
						<div class="tab-pane fade" id="day5" role="tabpanel" aria-expanded="true">
							<p class="text-red u-my-40 text-center u-fw-600 u-fs-22">
								Friday, 14 September 2018
							</p>
							<ul class="list-unstyled u-fw-600">
								<li class="d-flex align-items-center mb-2">
									<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
									Departure
								</li>
							</ul>
						</div> <!-- END tab-pane-->
					</div> <!-- END tab-content-->

	 			</div> <!-- END col-12 -->
	 		</div> <!-- END row-->
	 	</div> <!-- END container-->
 	</section>

{{--  	<div class="tab-pane fade active show rounded" id="day1" role="tabpanel" aria-expanded="true">
		<p class="text-red u-my-40 text-center u-fw-600 u-fs-22">
			Monday, 10 September 2018
		</p>
		<table class="table table-responsive text-center text-md-left">
			<thead class="bg-primary border-0 text-white">
				<tr>
					<th scope="col" class="text-center text-md-left">Session</th>
					<th scope="col" class="text-center text-md-left">Speakers</th>
					<th scope="col" class="text-center text-md-left">Time</th>
					<th scope="col" class="text-center text-md-left">Venue</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">Career with WordPress</th>
					<td>Peter Spenser</td>
					<td>08:45 AM</td>
					<td>Room 303</td>
				</tr>
				<tr>
					<th scope="row">Introduction to javaScript</th>
					<td>Misko</td>
					<td>09:00 AM</td>
					<td>Room 203</td>
				</tr>
				<tr>
					<th scope="row">WP Plugins Installation</th>
					<td>John abrahamov</td>
					<td>11:11 AM</td>
					<td>Room 12</td>
				</tr>
				<tr>
					<th scope="row">How to become a good developer</th>
					<td>Jafry way</td>
					<td>10:45 AM</td>
					<td>Room 07</td>
				</tr>
				<tr>
					<th scope="row">How to become a good entreprener</th>
					<td>Andy</td>
					<td>11:00 AM</td>
					<td>Room 259</td>
				</tr>
			</tbody>
		</table>
	</div> --}} <!-- END tab-pane-->