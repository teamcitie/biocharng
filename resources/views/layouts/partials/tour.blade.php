<section id="work">
	<div class="container">

		<div class="row">
			<div class="col-12 text-center">
				<h2 class="h1">
					Conference Tour
				</h2>
				<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40 mx-auto"></div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-7 mx-auto mt-5">
			</div>

			<div class="col-lg-10 mx-auto mt-5">
				<div class="tab-content tab__slider">

					<div class="tab-pane fade show active" id="tab2_2" role="tabpanel" aria-labelledby="home-tab">
						<div data-init="carousel" data-slick-margin="15" data-slick='{
								"slidesToShow":2,
								"slidesToScroll":1,
								"arrows":true,
								"prevArrow":"<div class=\"slick-prev box-shadow-v1\"><i class=\"fa fa-angle-left\"></i></div>",
								"nextArrow":"<div class=\"slick-next box-shadow-v1\"><i class=\"fa fa-angle-right\"></i></div>",
								"responsive": [
									{
									 "breakpoint":1024,
									 "settings":{
										"arrows":false,
										"dots":true,
										"dotsClass":"slick-dots text-center mt-4"
									 }
									},
									{
									 "breakpoint":700,
									 "settings":{
										"arrows":false,
										"slidesToShow":1,
										"dots":true,
										"dotsClass":"slick-dots text-center mt-4"
									 }
									}
								]
							}'>
							<div class="card">
								<img src="assets/img/tour/oopl.jpg" alt="">
								<div class="py-4">
									<h3>
									<a href="#">Olusegun Obasanjo Presidential Library</a>
								</h3>
								<p>
									{{-- Branding Design --}}
								</p>
								</div>
							</div>  <!-- END card-->
							<div class="card">
								<img src="assets/img/tour/olumo.jpg" alt="">
								<div class="py-4">
									<h3>
									<a href="#">Olumo Rock</a>
								</h3>
								<p>
									{{-- B2B marketing --}}
								</p>
								</div>
							</div>  <!-- END card-->
							<div class="card">
								<img src="assets/img/tour/oopl2.jpg" alt="">
								<div class="py-4">
									<h3>
									<a href="#">OOPL</a>
								</h3>
								<p>
									{{-- B2B marketing --}}
								</p>
								</div>
							</div> <!-- END card-->
							<div class="card">
								<img src="assets/img/tour/adire.jpg" alt="">
								<div class="py-4">
									<h3>
									<a href="#">Adire Market Itoku</a>
								</h3>
								<p>
									B2B marketing
								</p>
								</div>
							</div> <!-- END card-->
							<div class="card">
								<img src="assets/img/tour/oopl3.jpg" alt="">
								<div class="py-4">
									<h3>
									<a href="#">OOPL</a>
								</h3>
								<p>
									{{-- B2B marketing --}}
								</p>
								</div>
							</div> <!-- END card-->
							<div class="card">
								<img src="assets/img/tour/adire.jpg" alt="">
								<div class="py-4">
									<h3>
									<a href="#">Adire Market</a>
								</h3>
								<p>
									{{-- B2B marketing --}}
								</p>
								</div>
							</div> <!-- END card-->
						</div>
					</div> <!-- END tab-pane-->

				</div> <!-- END tab-content-->
			</div> <!-- END col-lg-10 mx-auto mt-5-->
		</div> <!-- END row-->
	</div> <!-- END container-->
</section>