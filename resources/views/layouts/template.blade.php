<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from echotheme.com/fury-v1.3/event.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 May 2018 15:33:26 GMT -->
	<head>

	  	<!--Meta-->
	  	<meta charset="UTF-8">
	  	<meta http-equiv="x-ua-compatible" content="ie=edge">
	  	<meta name="description" content="A complete landing page solution for any business">
		<meta name="viewport" content="width=device-width, initial-scale=1">

	  	<!--Favicon-->
	  	<link rel="icon" href="/assets/img/favicon/bin.ico">

	  	<!-- Title-->
	  	@yield('title')

	  	<!--Google fonts-->
	  	<link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700%7COpen+Sans:400,600,700" rel="stylesheet">

		<!--Icon fonts-->
		<link rel="stylesheet" href="/assets/vendor/strokegap/style.css">
		<link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/assets/vendor/linearicons/style.css">

	  <!-- Stylesheet-->
	  <!--
	// ////////////////////////////////////////////////
	// To Reduce server request and improved page speed drastically all third-party plugin bundle in /assets/css/bundle.css
	// If you wanna add those manually bellow is the sequence
	// ///////////////////////////////////////////////
	-->
	<!--  <link rel="stylesheet" href="/assets/vendor/bootstrap/dist/css/bootstrap.min.css">
	  <link rel="stylesheet" href="/assets/vendor/slick-carousel/slick/slick.css">
	  <link rel="stylesheet" href="/assets/vendor/fancybox/dist/jquery.fancybox.min.css">
	  <link rel="stylesheet" href="/assets/vendor/animate.css/animate.min.css">-->

	  	<link rel="stylesheet" href="/assets/css/bundle.css">
	  	<link rel="stylesheet" href="/assets/css/style.css">

	</head>

<body id="top">
	@include('vendor.sweetalert.cdn')
	@include('vendor.sweetalert.view')
	@include('layouts.extras.header')
	 <!-- END header -->
	@include('layouts.partials.alert')
	@yield('content')






	@include('layouts.extras.footer')
	<!-- END footer-->

		<div class="scroll-top bg-white box-shadow-v1">
			<i class="fa fa-angle-up" aria-hidden="true"></i>
		</div>


		<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
		<script src="/assets/vendor/popper.js/dist/popper.min.js"></script>
		<script src="/assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/assets/vendor/slick-carousel/slick/slick.min.js"></script>
		<script src="/assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>
		<script src="/assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
		<script src="/assets/vendor/isotope/dist/isotope.pkgd.min.js"></script>
		<script src="/assets/vendor/parallax.js/parallax.min.js"></script>
		<script src="/assets/vendor/wow/dist/wow.min.js"></script>
		<script src="/assets/vendor/vide/dist/jquery.vide.min.js"></script>
		<script src="/assets/vendor/typed.js/lib/typed.min.js"></script>
		<script src="/assets/vendor/appear-master/dist/appear.min.js"></script>
		<script src="/assets/vendor/jquery.countdown/dist/jquery.countdown.min.js"></script>
		<script src="/assets/js/fury.js"></script>

		<script src="https://maps.googleapis.com/maps/api/js?&amp;key=AIzaSyB0uuKeEkPfAo7EUINYPQs3bzXn7AabgJI"></script>

</body>

</html>