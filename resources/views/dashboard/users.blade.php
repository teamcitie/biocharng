@extends('dashboard.template')

@section('title')
	<title>Dashboard | Users</title>
@endsection

@section('breadcrumb')
	Papers
@endsection

@section('content')
	<div class="">
		<div class="card">
		    <div class="card-body">
		        <h4 class="card-title">Users table</h4>
		        <div class="row">
		            <div class="col-12">
		                <div class="table-responsive">
		                    <table id="order-listing" class="table">
		                        <thead>
		                            <tr>
		                                <th>Order #</th>
		                                <th>Fullname</th>
		                                <th>Phone Number</th>
		                                <th>Institution</th>
		                                {{-- <th>BIN No.</th> --}}
		                                <th>Date</th>
		                                <th>Email</th>
		                                <!-- <th>Actions</th> -->
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	@foreach($users as $user)
			                            <tr>
			                                <td>{{ $loop->index + 1 }}</td>
			                                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
			                                <td>{{ $user->phone_number }}</td>
			                                <td>{{ $user->institution }}</td>
			                                {{-- <td>{{ $user->member_number }}</td> --}}
			                                <td>{{ $user->created_at->diffForHumans() }}</td>
			                                <td>{{ $user->email }}</td>
			                                <!-- <td><button class="btn btn-outline-primary">View</button></td> -->
			                            </tr>
		                            @endforeach
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>

@endsection