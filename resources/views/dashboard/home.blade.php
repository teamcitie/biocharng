@extends('dashboard.template')

@section('title')
	<title>Dashboard | Home</title>
@endsection

@section('breadcrumb')
	Dashboard
@endsection

@section('content')
	<div class="row">
		<div class="col-md-4 d-flex align-items-stretch">
			<div class="row flex-grow-1 w-100">
                <div class="col-12 grid-margin stretch-card">
                  	<div class="card">
	                    <div class="card-body d-flex justify-content-center">
	                      <div class="d-flex align-items-center">
	                        <div class="bg-primary p-3 text-white rounded mr-3">
	                          <i class="mdi mdi-file-outline icon-md"></i>
	                        </div>
	                        <div>
	                          	<a class="nav-link" href="{{ route('dashboard.papers') }}">
	                        		<h3>{{ $papers->count() }}</h3>
	                          		<p class="mb-0">Abstract Submited</p>
	                        	</a>
	                        </div>
	                      </div>
	                    </div>
          			</div>
                </div>
      		</div>
		</div>

		<div class="col-md-4 d-flex align-items-stretch">
			<div class="row flex-grow-1 w-100">
                <div class="col-12 grid-margin stretch-card">
                  	<div class="card">
	                    <div class="card-body d-flex justify-content-center">
	                      <div class="d-flex align-items-center">
	                        <div class="bg-primary p-3 text-white rounded mr-3">
	                          <i class="mdi mdi-account icon-md"></i>
	                        </div>
	                        <div>
	                        	<a class="nav-link" href="{{ route('dashboard.users') }}">
	                        		<h3>{{ $users->count() }}</h3>
	                          		<p class="mb-0">Registered Users</p>
	                        	</a>
	                        </div>
	                      </div>
	                    </div>
          			</div>
                </div>
      		</div>
		</div>
	</div>
@endsection