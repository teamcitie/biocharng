@extends('dashboard.template')

@section('title')
	<title>Dashboard | Papers</title>
@endsection

@section('breadcrumb')
	Papers
@endsection

@section('content')
	<div class="row">
		@foreach($papers as $paper)
			{{-- {{ $file = json_encode($paper->file) }} --}}
			<div class="col-md-4 d-flex align-items-stretch">
				<div class="row flex-grow-1 w-100">
	                <div class="col-12 grid-margin stretch-card">
	                  	<div class="card">
		                    <div class="card-body d-flex justify-content-center">
		                      <div class="d-flex align-items-center">
		                        <div class="bg-primary p-3 text-white rounded mr-3">
		                          <i class="mdi mdi-file-outline icon-lg"></i>
		                        </div>
		                        <div>
		                          	{{-- <a class="nav-link" href="#"> --}}
		                        		<p>Uploaded by</p>
		                          		<h6 class="mb-0">{{ $paper->fullname }}</h6>
		                          		<p>{{ $paper->created_at->diffForHumans() }}</p>
		                          		<p><a class="nav-link btn btn-outline-primary btn-sm btn-icon-text" href="{{ $paper->getUserImg() }}"><i class="mdi mdi-download btn-icon-prepend"></i> Download</a></p>
		                        	{{-- </a> --}}
		                        </div>
		                      </div>
		                    </div>
	          			</div>
	                </div>
	      		</div>
			</div>
		@endforeach

	</div>
@endsection