<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">

		<li class="nav-item {{ Request::is('/dashboard/') ? 'active' : ' ' }}">
	        <a class="nav-link" href="{{ route('dashboard') }}">
	          	<i class="mdi mdi-home-outline menu-icon"></i>
          		<span class="menu-title">Dashboard</span>
	        </a>
  		</li>

  		<li class="nav-item {{ Request::is('dashboard/users') ? 'active' : ' ' }}">
	        <a class="nav-link {{ Request::is('dashboard/users') ? 'active' : ' ' }}" href="{{ route('dashboard.users') }}">
	          	<i class="mdi mdi mdi-book-multiple menu-icon"></i>
      			<span class="menu-title {{ Request::is('dashboard/users') ? 'active' : ' ' }}">Users</span>
	        </a>
  		</li>

  		<li class="nav-item {{ Request::is('dashboard/papers') ? 'active' : ' ' }}">
	        <a class="nav-link {{ Request::is('dashboard/papers') ? 'active' : ' ' }}" href="{{ route('dashboard.papers') }}">
	          	<i class="mdi mdi mdi-book-multiple menu-icon"></i>
      			<span class="menu-title {{ Request::is('dashboard/papers') ? 'active' : ' ' }}">Papers</span>
	        </a>
  		</li>
	</ul>
</nav>