<!DOCTYPE html>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		{{-- <title>Gleam Admin</title> --}}
		@yield('title')
		<!-- plugins:css -->
		<link rel="stylesheet" href="/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="/admin/vendors/css/vendor.bundle.base.css">
		<link rel="stylesheet" href="/admin/vendors/css/vendor.bundle.addons.css">
		<!-- endinject -->
		<!-- plugin css for this page -->
		<!-- End plugin css for this page -->
		<!-- inject:css -->
		<link rel="stylesheet" href="/admin/css/style.css">
		<!-- endinject -->
		<link rel="shortcut icon" href="/admin/images/favicon.png" />
	</head>
	<body>

		@include('dashboard.partials.nav')

		{{-- Main body Wrapper --}}
		<div class="container-fluid page-body-wrapper">
			@include('dashboard.partials.sidebar')

			<div class="main-panel">
				<div class="content-wrapper">
					<div class="page-header">
			            <h3 class="page-title">
			              Dashboard
			            </h3>
			            <nav aria-label="breadcrumb">
			              	<ol class="breadcrumb">
				                <li class="breadcrumb-item"><a href="#">Home</a></li>
				                <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb')</li>
			              	</ol>
			            </nav>
	          		</div>

	          		@yield('content')
				</div>

          		<footer class="footer">
		          	<div class="d-sm-flex justify-content-center justify-content-sm-between">
            			<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright &copy; {{ date('Y') }} <a href="https://www.biocharng.com/" target="_blank">BIOCHAR Nigeria</a>. All rights reserved.</span>
            			<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          			</div>
		        </footer>
			</div>


		</div>
		<!-- plugins:js -->
		<script src="/admin/vendors/js/vendor.bundle.base.js"></script>
		<script src="/admin/vendors/js/vendor.bundle.addons.js"></script>
		<!-- endinject -->
		<!-- Plugin js for this page-->
		<!-- End plugin js for this page-->
		<!-- inject:js -->
		<script src="/admin/js/off-canvas.js"></script>
		<script src="/admin/js/hoverable-collapse.js"></script>
		<script src="/admin/js/misc.js"></script>
		<script src="/admin/js/settings.js"></script>
		<script src="/admin/js/todolist.js"></script>
		<!-- endinject -->
		<!-- Custom js for this page-->
		<script src="/admin/js/dashboard.js"></script>
		<!-- Custom js for this page-->
  		<script src="/admin/js/data-table.js"></script>
	</body>
</html>