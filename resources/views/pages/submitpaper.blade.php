@extends('layouts.template')

@section('title')
	<title>BIN - Submit Paper</title>
@endsection

@section('content')
	<section data-dark-overlay="10" data-init="parallax" class="u-py-100 u-pt-lg-200 u-pb-lg-150 u-flex-center" style="background:#28a745;">
	  	<div class="container">
		    <div class="row">
		    	<div class="col-12 text-center text-white">
		    		<h1 class="text-white">Submit paper.</h1>
		    		<div class="u-h-4 u-w-50 bg-white rounded mx-auto my-4"></div>
		    		<p class="lead">
		    			Please fill appropraitely!
		    		</p>
		    	</div>
		    </div> <!-- END row-->
	  	</div> <!-- END container-->
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 mx-auto text-center">
					<h2 class="h1">
						Welcome {{ Auth::user()->first_name }}
					</h2>
					<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40 mx-auto"></div>
					<p>
						You can upload either your abstract or the full paper.
					</p>
				</div>
			</div> <!-- END row-->
			<div class="row">
				<div class="col-lg-7 text-center mt-5 mx-auto">
					<form action="{{ route('paper.submit') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6">
								<label class="text-left"><strong>Fullname:</strong></label>
								<input name="fullname" type="text" class="form-control u-rounded-50 p-3 u-mb-30 {{ $errors->has('fullname') ? 'is-invalid' : ' ' }}" placeholder="Mr. Peter English" required="" value="{{ Request::old('fullname') ?: '' }}">
								@if ($errors->has('fullname'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('fullname') }}*
							    	</div>
							    @endif
							</div>
							<div class="col-md-6">
								<label class="text-left"><strong>Email:</strong></label>
								<input name="email" type="text" class="form-control u-rounded-50 p-3 u-mb-30 {{ $errors->has('email') ? 'is-invalid' : ' ' }}" placeholder="user@email.com" required="" value="{{ Request::old('email') ?: '' }}">
								@if ($errors->has('email'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('email') }}*
							    	</div>
							    @endif
							</div>
							<div class="col-md-6">
								<label class="text-left"><strong>Telephone:</strong></label>
								<input name="phone_number" type="text" class="form-control u-rounded-50 p-3 u-mb-30 {{ $errors->has('phone_number') ? 'is-invalid' : ' ' }}" placeholder="+2348012345678" required="" value="{{ Request::old('phone_number') ?: '' }}">
								@if ($errors->has('phone_number'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('phone_number') }}*
							    	</div>
							    @endif
							</div>
							<div class="col-md-6">
								<label class="text-left"><strong>Select Sub-Theme</strong></label>
								<select class="form-control u-mb-30" name="section">
									<option>Select a sub theme</option>
									<option class="form-control" value="Biochar Production and Technology for Renewable Energy">Biochar Production and Technology for Renewable Energy</option>
									<option class="form-control" value="Biochar a Tool for Sustainable Soil Improvement and Crop Production">Biochar a Tool for Sustainable Soil Improvement and Crop Production</option>
									<option class="form-control" value="Biochar for Sustainable Environmental Management">Biochar for Sustainable Environmental Management</option>
									<option class="form-control" value="Biochar an Instrument of Livestock Production and Aquaculture">Biochar an Instrument of Livestock Production and Aquaculture</option>
									<option class="form-control" value="Strategies for Adoption and Utilization of Biochar Technology for Farmers">Strategies for Adoption and Utilization of Biochar Technology for Farmers</option>
								</select>
							</div>
							<div class="col-md-6 u-mb-30">
								<label class="text-left"><strong>Select file:</strong></label>
								<input type="file" name="paper" class="form-control {{ $errors->has('paper') ? 'is-invalid' : '' }}" value="{{ Request::old('paper') ?: '' }}">
								@if ($errors->has('paper'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('paper') }}*
							    	</div>
							    @endif
							</div>
						</div>
						{{-- <textarea class="form-control u-rounded-15 p-3 u-mb-30" rows="6" placeholder="Youe text..." required=""></textarea> --}}
						<button class="btn btn-rounded btn-success u-w-170" type="submit">Submit</button>
					</form>
				</div> <!-- END col-lg-7 -->
			</div> <!-- END row-->
		</div> <!-- END container-->
	</section>
@endsection