@extends('layouts.template')

@section('title')
	<title>Welcome to BIN</title>
@endsection

@section('content')
	<section class="u-py-100 u-h-100vh u-flex-center" data-dark-overlay="6" style="background:url(assets/img/about/compost-rico.jpg) no-repeat; background-size: cover; background-position: top center;">
	  	<div class="container">
		    <div class="row text-center">
		      <div class="col-12 u-mt-30">
		        <h1 class="display-4 u-fw-600 text-white u-mb-0">
		          	4th Annual BIN Conference
		        </h1>
		        <h2 class="my-4 u-mt-10">
		        	<span class="text-primary">10 - 14 SEPTEMBER, 2018,</span><br> <span class="text-white">FEDERAL UNIVERSITY OF AGRICULTURE ABEOKUTA, NIGERIA</span>
		        </h2>
		        <h4 class="display-4 u-fw-600 text-yellow u-mb-10 u-mt-30">
		        	Theme:
		        </h4>
		        <h2 class="h1 u-fw-600 text-white u-mb-20">
		          BIOCHAR TECHNOLOGY: PANACEA FOR SUSTAINABLE AGRICULTURAL AND ENVIRONMENTAL PROTECTION
		        </h2>
		        <a href="{{ route('paper.submit') }}" class="btn btn btn-rounded btn-green  px-md-5">
		        	Submit Paper
		        </a>
		        {{-- <a href="#" class="btn btn btn-rounded btn-white ml-3 px-md-5">
		        	Register
		        </a> --}}
		      </div> <!-- END col-lg-6-->
		    </div> <!-- END row-->
	  	</div> <!-- END container-->
	</section> <!-- END intro-hero-->

	<section id="about">
	  <div class="container">
	    <div class="row align-items-center">
	    	<div class="col-md-5">
					<div class="card box-shadow-v2 bg-white u-of-hidden text-center">
						{{-- <h2 class="bg-primary m-0 py-3 text-white">Book Your Seat </h2> --}}
						{{-- <form action="#" method="POST" class="p-4 p-md-5">

							<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<span class="icon icon-User text-primary"></span>
								</div>
								<input type="text" class="form-control border-0 p-3" placeholder="Your fullname" required>
							</div>

							<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<span class="icon icon-Mail text-primary"></span>
								</div>
								<input type="email" class="form-control border-0 p-3" placeholder="Your email" required>
							</div>
							<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<span class="icon icon-Phone text-primary"></span>
								</div>
								<input type="tel" class="form-control border-0 p-3" placeholder="Phone" required>
							</div>

							<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<span class="icon icon-Dollar text-primary"></span>
								</div>
								<select class="form-control border-0 p-3 u-h-auto">
									<option>$19</option>
									<option>$29</option>
									<option>$99</option>
									<option>$199</option>
								</select>
							</div>
							<button class="btn btn-primary btn-rounded px-5">
								Buy Now
							</button>
						</form> --}}
						<img src="/assets/img/black.jpg" class="img-fluid">
					 </div>
	    	</div> <!-- END col-md-5-->
	    	<div class="col-md-7 col-lg-6 ml-auto mt-5">
	    		<h2>Sub-Themes BIOCHAR 2018</h2>
	    		<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40"></div>
	    		<ul class="list-unstyled u-fw-600">
					<li class="d-flex align-items-center mb-2">
						<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
						Biochar Production and Technology for Renewable Energy
					</li>
					<li class="d-flex align-items-center mb-2">
						<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
						Biochar a Tool for Sustainable Soil Improvement and Crop Production
					</li>
					<li class="d-flex align-items-center mb-2">
						<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
						Biochar for Sustainable Environmental Management
					</li>
					<li class="d-flex align-items-center mb-2">
						<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
						Biochar an Instrument of Livestock Production and Aquaculture
					</li>
					<li class="d-flex align-items-center mb-2">
						<span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"></span>
						Strategies for Adoption and Utilization of Biochar Technology for Farmers
					</li>
				</ul>
	    		<a href="#" class="btn btn-red btn-rounded mt-4" data-scrollto="schedule">View Schedule</a>
	    	</div>
	    </div> <!-- END row-->
	  </div> <!-- END container-->
	</section> <!-- END section-->

	<hr class="m-0">

	@include('layouts.partials.schedule')
	{{-- <section>
	  <div class="container">
	   <div class="row">
				<div class="col-md-6 mb-4">
					<h2 class="mb-4">
						Why Blofu Conference 2018?
					</h2>
					<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40"></div>
					<p>
						Nam liber tempor cum soluta nobis eleifend option congue is nihil imper  per tem por legere me that doming vulputate velit esse molestie  possim wisi enim ad placerat facer possim assum minim there veniam, nostrud exerci tation ullamcorper quis nostrud.
					</p>
					<ul class="list-unstyled u-fw-600 u-lh-2 u-mt-30">
						<li><i class="fa fa-check text-primary mr-2"></i>Professional and easy-to-use software</li>
						<li><i class="fa fa-check text-primary mr-2"></i>Setup and installations takes ten minutes</li>
						<li><i class="fa fa-check text-primary mr-2"></i>Perfect for any device with pixel-perfect design</li>
				 </ul>
				</div> <!-- END col-md-6 -->
				<div class="col-md-6 mb-4">
					<h2 class="mb-4">
						Who Should Attend?
					</h2>
					<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40"></div>
					<p>
						Nam liber tempor cum soluta nobis eleifend option congue is nihil imper iper tem por legere me that doming vulputate velit esse molestie possim wisi enim ad placerat facer possim. Nam liber tempor cum soluta nobis eleifend option congue is nihil imper iper tem por legere.
					</p>
					<p>
						Nam liber tempor cum soluta nobis eleifend option congue is nihil imper iper tem por legere me that doming vulputate velit esse molestie possim wisi enim ad placerat facer possim.
					</p>
				</div> <!-- END col-md-6 -->
	   </div> <!-- END row-->
	  </div> <!-- END container-->
	</section>  --}}<!-- END section-->

	<section id="speakers" style="
		background: #ec008c;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #fc6767, #ec008c);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #fc6767, #ec008c); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */">
		  <div class="container">
			    <div class="row">
			    	<div class="col-12 text-center text-white">
			    		<h1 class="text-white">
			    			Keynote Speaker
			    		</h1>
			    		<div class="u-h-4 u-w-50 bg-white rounded mx-auto my-4"></div>
			    	</div>
			    </div> <!-- END row-->
				<div class="row align-items-center text-white">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-md-6 my-4">
								<img class="w-100 rounded box-shadow-v1" src="/assets/img/person/oyedele.jpg" alt="">
							</div>
							<div class="col-md-6 my-4">
								<h4 class="mx-auto my-4 mb-0 text-white">
									Prof. D. J Oedele
								</h4>
								<p class="text-white mb-0" style="color: #273f5b !important;">
									<span style="font-weight: 600;">Dean, Faculty of Agriculture</span>
									<br>
									Obafemi Awolowo University
									<br>
									Ile Ife, Osun state, Nigeria
								</p>
								<p class="my-4">
									{{-- A very sound father, professor of law and the Vice President of tthe Federal Republic of Nigeria. --}}
								</p>
								{{-- <ul class="list-inline social social-rounded social-default social-sm">
									<li class="list-inline-item">
										<a href="#" class="bg-red-opacity-0_5"><i class="fa fa-facebook"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-twitter"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-google-plus"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</li>
								</ul> --}}
							</div>
						</div> <!-- END row-->
					</div> <!-- END col-lg-6-->
					{{-- <div class="col-lg-6">
						<div class="row">
							<div class="col-md-6 my-4">
								<img class="w-100 rounded box-shadow-v1" src="/assets/img/person/placeholder.png" alt="">
							</div>
							<div class="col-md-6 my-4">
								<h4 class="mb-0 text-white">
									COMING SOON
								</h4>
								<p class="text-white mb-0">
									Minister of Agriculture
								</p>
								<p class="my-4">
									Nam liber tempor cum soluta nobis eleifend option congue is nihil they imper.
								</p>
								<ul class="list-inline social social-rounded social-default social-sm">
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-facebook"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-twitter"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-google-plus"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</li>
								</ul>
							</div>
						</div> <!-- END row-->
					</div> --}} <!-- END col-lg-6-->
					{{-- <div class="col-lg-6">
						<div class="row">
							<div class="col-md-6 my-4">
								<img class="w-100 rounded box-shadow-v1" src="/assets/img/person/placeholder.png" alt="">
							</div>
							<div class="col-md-6 my-4">
								<h4 class="mb-0 text-white">
									COMING SOON
								</h4>
								<p class="text-white mb-0">
									Minister of Power and steel
								</p>
								<p class="my-4">
									A bright mind and former governor of Lagos state.
								</p>
								<ul class="list-inline social social-rounded social-default social-sm">
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-facebook"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-twitter"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-google-plus"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</li>
								</ul>
							</div>
						</div> <!-- END row-->
					</div> --}} <!-- END col-lg-6-->
					{{-- <div class="col-lg-6">
						<div class="row">
							<div class="col-md-6 my-4">
								<img class="w-100 rounded box-shadow-v1" src="/assets/img/person/placeholder.png" alt="">
							</div>
							<div class="col-md-6 my-4">
								<h4 class="mb-0 text-white">
									COMING SOON
								</h4>
								<p class="text-white mb-0">
									Vice Chancellor FUNAAB
								</p>
								<p class="my-4">
									Nam liber tempor cum soluta nobis eleifend option congue is nihil they imper.
								</p>
								<ul class="list-inline social social-rounded social-default social-sm">
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-facebook"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-twitter"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-google-plus"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</li>
								</ul>
							</div>
						</div> <!-- END row-->
					</div> --}} <!-- END col-lg-6-->
				</div> <!-- END row-->
		  </div> <!-- END container-->
	</section>
	<section class="u-py-100" style="background:url(assets/img/about/bg6.jpg) no-repeat; background-size: cover; background-position: center center;">
 		<div class="overlay bg-black-opacity"></div>
 		<div class="container">
 			<div class="row">
 				<div class="col-12 text-center d-md-flex  align-items-center justify-content-center">
 					<h2 class="h1 text-white mx-md-5">
 						Have your jornal here?
 					</h2>
 					<a href="{{ route('paper.submit') }}" class="btn btn-white btn-rounded mt-4 mt-md-0">
 						Submit Paper
 					</a>
 				</div>
 			</div>
 		</div>
 	</section>

 	{{-- @include('layouts.partials.schedule') --}}

 	@include('layouts.partials.tour')

	<div class="container-fluid bg-gray-v1" id="map">
		<div class="row text-center">
			<div class="col-lg-7 mx-auto text-center">
				<h2 class="mb-4">
					Locate Us
				</h2>
				<div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-70 mx-auto"></div>
			</div>


			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.088070571182!2d3.435392914586311!3d7.230795144780177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103a36500345071b%3A0x1e0dd26e3facb70d!2sFederal+University+Of+Agriculture+Abeokuta!5e0!3m2!1sen!2sng!4v1525263569036" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>

	</div>


	<section data-dark-overlay="7" style="background: url(/assets/img/event/bg-2.jpg) no-repeat; background-size: cover;">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-4 mb-4">
					<div class="bg-white box-shadow-v1 p-5 rounded u-h-100p">
						<span class="icon icon-Pointer text-green u-fs-60"></span>
						 <h4 class="text-yellow my-4">
						 	Event Venue
						 </h4>
						 <p>
								Federal University of Agriculture, Abeokuta <br>
								(FUNAAB), <br>
								Ogun State, Nigeria
						 </p>
					</div>
				</div> <!-- END col-md-4 -->
				<div class="col-md-4 mb-4">
					<div class="bg-white box-shadow-v1 p-5 rounded u-h-100p">
						<span class="icon icon-Mail text-green u-fs-60"></span>
						 <h4 class="text-yellow my-4">
						 	Contact Information
						 </h4>
						 <p>
						 	0803 - 35793 70<br>
							babalolaoa@funaab.edu.ng<br>
							soretireaa@funaab.edu.ng<br>
							biocharnigeria@gmail.com
							<br>
						 </p>
					</div>
				</div> <!-- END col-md-4 -->
				<div class="col-md-4 mb-4">
					<div class="bg-white box-shadow-v1 p-5 rounded u-h-100p bg-white px-4 py-5 bg-red text-white">
						<span class="icon icon-Starship2 text-white u-fs-60"></span>
						 <h4 class="text-yellow my-4">
						 	Deadline
						 </h4>
						 <p class="h4 text-white">
							Date : 2018-09-10
						 </p>
					</div>
				</div> <!-- END col-md-4 -->
			</div> <!-- END row-->
			{{-- <div class="row u-mt-80">
				<div class="col-lg-7 mx-auto text-center">
					<h2 class="text-white mb-4">
						Sign Up For Our Newsletter
					</h2>
					<div class="u-h-4 u-w-50 bg-white rounded mt-4 u-mb-70 mx-auto"></div>
					<form class="form-inline justify-content-center subsribe-rounded">
							<div class="form-group">
								<input type="email" class="form-control px-4 u-rounded-50 u-h-50 bg-transparent" placeholder="Enter your email address" required="">
							</div>
							<button type="submit" class="btn btn-rounded btn-red u-h-50">
								Subscribe Now
							</button>
						</form>
				</div>
			</div> --}}
		</div> <!-- END container-->
	</section>
@endsection