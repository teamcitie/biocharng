@extends('layouts.template')

@section('title')
	<title>BIN - Signup</title>
@endsection

@section('content')
	<section data-dark-overlay="10" data-init="parallax" class="u-py-100 u-pt-lg-200 u-pb-lg-150 u-flex-center" style="background:#28a745;">
	  	<div class="container">
		    <div class="row">
		    	<div class="col-12 text-center text-white">
		    		<h1 class="text-white">Sign up</h1>
		    		<div class="u-h-4 u-w-50 bg-white rounded mx-auto my-4"></div>
		    		<p class="lead">
		    			Welcome. Please fill appropraitely!
		    		</p>
		    	</div>
		    </div> <!-- END row-->
	  	</div> <!-- END container-->
	</section>

	<section style="background:#fff; background-position: center center;">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto">
					{{-- <h2 class="h1 text-center text-white u-mb-30">
						Welcome <br> Please fill appropraitely
					</h2> --}}
					<div class="u-h-4 u-w-50 bg-white rounded u-mb-70 mx-auto"></div>
					<form action="{{ route('auth.signup') }}" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6 u-mb-30">
								<label><strong>Your Firstname:</strong></label>
								<input class="form-control my-input {{ $errors->has('first_name') ? 'is-invalid' : '' }}" type="text" name="first_name" placeholder="Adewale" value="{{ Request::old('first_name') ?: '' }}" required>
								@if ($errors->has('first_name'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('first_name') }}*
							    	</div>
							    @endif
							</div>

							<div class="col-md-6 u-mb-30">
								<label><strong>Your Lastname:</strong></label>
								<input class="form-control my-input {{ $errors->has('last_name') ? 'is-invalid' : '' }}" type="text" name="last_name" placeholder="Akanni" value="{{ Request::old('last_name') ?: '' }}" required>
								@if ($errors->has('last_name'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('last_name') }}*
							    	</div>
							    @endif
							</div>

							<div class="col-md-6 u-mb-30">
								<label><strong>Email address:</strong></label>
								<input class="form-control my-input {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" placeholder="user@example.com" value="{{ Request::old('email') ?: '' }}" required>
								@if ($errors->has('email'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('email') }}*
							    	</div>
							    @endif
							</div>
							<div class="col-md-6 u-mb-30">
								<label><strong>Phone number:</strong></label>
								<input class="form-control my-input {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" type="tel" name="phone_number" placeholder="+2348012345678" value="{{ Request::old('phone_number') ?: '' }}" required>
								@if ($errors->has('phone_number'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('phone_number') }}*
							    	</div>
							    @endif
							</div>
							<div class="col-md-6 u-mb-30">
								<label><strong>Institution:</strong></label>
								<input class="form-control my-input {{ $errors->has('institution') ? 'is-invalid' : '' }}" type="text" name="institution" placeholder="Your institution" value="{{ Request::old('institution') ?: '' }}" required>
								@if ($errors->has('institution'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('institution') }}*
							    	</div>
							    @endif
							</div>
							<div class="col-6 u-mb-30">
								<label><strong>Membership no: </strong>(<em> for BIN members only</em>)</label>
								<input class="form-control my-input" type="text" name="member_number" placeholder="BINOO2" value="{{ Request::old('member_number') ?: '' }}">
							</div>

							<div class="col-6 u-mb-30">
								<label><strong>Password: </strong>(<em> At least 6 characters</em>)</label>
								<input class="form-control my-input {{ $errors->has('password') ? 'is-invalid' : '' }}" type="Password" name="password" placeholder="" value="{{ Request::old('password') ?: '' }}" required>
								@if ($errors->has('password'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('password') }}*
							    	</div>
							    @endif
							</div>

							<div class="col-6 u-mb-30">
								<label><strong>Password confirmation: </strong></label>
								<input class="form-control my-input {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" type="password" name="password_confirmation" placeholder="" required>
								@if ($errors->has('password_confirmation'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('password_confirmation') }}*
							    	</div>
							    @endif
							</div>
						</div> <!-- END row-->
						<div class="text-center">
							<a href="{{ route('auth.signin') }}" style="text-decoration: underline;" class="text-primary">Sign In</a>&nbsp;&nbsp;&nbsp;<button class="btn btn-white">Sign up</button>
						</div>
					</form>
				</div> <!-- END col-lg-8-->
			</div> <!-- END row-->
		</div> <!-- END container-->
	</section>
@endsection