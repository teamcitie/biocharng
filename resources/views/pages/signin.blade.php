@extends('layouts.template')

@section('title')
	<title>BIN - Signin</title>
@endsection

@section('content')
	<section data-dark-overlay="5" class="u-h-100vh u-flex-center" style="background:url(assets/img/about/compost-rico.jpg) no-repeat; background-size:cover; background-position: top center;">
		<div class="container">

		    <div class="row">
		    	<div class="col-lg-5 m-auto text-center">
					<div class="card box-shadow-v2 bg-white u-of-hidden">
					 	<h2 class="bg-success m-0 py-3 text-white">Login</h2>
				 		<div class="p-4 p-md-5">

							<form action="{{ route('auth.signin') }}" method="POST">
								{{ csrf_field() }}
								<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
									<div class="input-group-addon bg-white border-0 pl-4 pr-0">
										<span class="icon icon-User text-primary"></span>
									</div>
									{{-- <input type="email" class="form-control border-0 p-3" placeholder="user@email.com"> --}}
									<input class="form-control border-0 p-3 {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" placeholder="user@example.com" value="{{ Request::old('email') ?: '' }}" required>
									@if ($errors->has('email'))
								    	<div class="invalid-feedback">
								    		{{ $errors->first('email') }}*
								    	</div>
								    @endif
								</div>

								<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
									<div class="input-group-addon bg-white border-0 pl-4 pr-0">
										<span class="icon icon-ClosedLock text-primary"></span>
									</div>
									{{-- <input type="password" class="form-control border-0 p-3" placeholder="********"> --}}
									<input class="form-control border-0 p-3 {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" placeholder="******" required>
									@if ($errors->has('password'))
								    	<div class="invalid-feedback">
								    		{{ $errors->first('password') }}*
								    	</div>
								    @endif
								</div>
								<div class="d-flex justify-content-between align-items-center">
									<label class="custom-control custom-checkbox text-left">
										<input type="checkbox" class="custom-control-input" name="remember">
										<span class="custom-control-indicator mt-1"></span>
										<span class="custom-control-description">Remember me</span>
									</label>
									{{-- <a href="#">Forgot password?</a> --}}
								</div>
								<button class="btn btn-success btn-rounded u-mt-20 u-w-170">
									Login Now
								</button>
							</form>

							<p>
								Don't have an account? <a href="{{ route('auth.signup') }}" class="text-primary">Sign up</a>
							</p>
							<p>
								<a href="{{ route('password.reset') }}" class="text-primary">Forgot my password</a>
							</p>
				 		</div> <!-- END p-4 p-md-5-->
					</div>  <!-- END card-->
		     	</div> <!-- END col-lg-5-->
		    </div> <!-- END row-->
		</div> <!-- END container-->

	</section> <!-- END intro-hero-->
@endsection