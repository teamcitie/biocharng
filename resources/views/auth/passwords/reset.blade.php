@extends('layouts.template')

@section('title')
	<title>BIN - Signup</title>
@endsection

@section('content')
	<section data-dark-overlay="10" data-init="parallax" class="u-py-100 u-pt-lg-200 u-pb-lg-150 u-flex-center" style="background:#28a745;">
	  	<div class="container">
		    <div class="row">
		    	<div class="col-12 text-center text-white">
		    		<h1 class="text-white">Reset your password</h1>
		    		<div class="u-h-4 u-w-50 bg-white rounded mx-auto my-4"></div>
		    		<p class="lead">
		    			Welcome. Please fill appropraitely!
		    		</p>
		    	</div>
		    </div> <!-- END row-->
	  	</div> <!-- END container-->
	</section>

	<section style="background:#fff; background-position: center center;">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto">
					{{-- <h2 class="h1 text-center text-white u-mb-30">
						Welcome <br> Please fill appropraitely
					</h2> --}}
					<div class="u-h-4 u-w-50 bg-white rounded u-mb-70 mx-auto"></div>
					<form action="" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6 u-mb-30">
								<label><strong>Email address:</strong></label>
								<input class="form-control my-input {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" placeholder="user@example.com" value="{{ Request::old('email') ?: '' }}" required>
								@if ($errors->has('email'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('email') }}*
							    	</div>
							    @endif
							</div>

							<div class="col-6 u-mb-30">
								<label><strong>Password: </strong>(<em> At least 6 characters</em>)</label>
								<input class="form-control my-input {{ $errors->has('password') ? 'is-invalid' : '' }}" type="Password" name="password" placeholder="" value="{{ Request::old('password') ?: '' }}" required>
								@if ($errors->has('password'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('password') }}*
							    	</div>
							    @endif
							</div>

							<div class="col-6 u-mb-30">
								<label><strong>Password confirmation: </strong></label>
								<input class="form-control my-input {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" type="password" name="password_confirmation" placeholder="" required>
								@if ($errors->has('password_confirmation'))
							    	<div class="invalid-feedback">
							    		{{ $errors->first('password_confirmation') }}*
							    	</div>
							    @endif
							</div>
						</div> <!-- END row-->
						<div class="text-center">
							<button class="btn btn-white">Update password</button>
						</div>
					</form>
				</div> <!-- END col-lg-8-->
			</div> <!-- END row-->
		</div> <!-- END container-->
	</section>
@endsection