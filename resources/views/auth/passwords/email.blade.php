@extends('layouts.template')

@section('title')
	<title>BIN - Signup</title>
@endsection

@section('content')
	<form method="POST" action="{{ route('post.reset')}}">
		{{ csrf_field() }}
		<section class="u-h-100vh u-flex-center">
		  <div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h1 class="text-primary u-fs-60 u-fs-md-150 "><i class="fa fa-frown-o"></i></h1>
						<h2 class="mb-4">
							Sorry! You could not sign in <i class="fa fa-frown-o text-yellow" aria-hidden="true"></i>
						</h2>
						<p>Please type the email address you used at registration</p>
						<div class="u-flex-center">
							<p class="col-6">
								<input class="form-control my-inputs my-input" type="email" name="email" placeholder="user@email.com" required>
								@if (session('alert'))
							    	<div class="invalid-feedback">
							    		{{ session('alert') }}*
							    	</div>
							    @endif
							</p>
						</div>
						<button class="btn btn-rounded btn-primary mt-1">Send reset link</button>
					</div>
		    </div> <!-- END row-->
		  </div> <!-- END container-->

		</section> <!-- END intro-hero-->
	</form>
@endsection