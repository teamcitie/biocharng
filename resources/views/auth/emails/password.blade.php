<style>
	body {
		text-align: center;
		font-family: 'Open Sans', sans-serif;
	}
	a {
		text-decoration: none;
	}
	.my-button {
		border: none;
	    background: #02b3e4;
	    padding: 1.2em 4em;
	    border-radius: 4px !important;
	    box-shadow: 4px 4px 20px rgba(0, 0, 0, 0.1);
	    color: #fff;
	    cursor: pointer;
	    display: inline-block;
	    font-family: 'Open Sans', sans-serif;
	    font-size: 12px;
	    font-weight: 600;
	    -webkit-font-smoothing: antialiased;
	    letter-spacing: 0.165em;
	    max-width: 100%;
	    overflow: hidden;
	    text-align: center;
	    text-overflow: ellipsis;
	    text-transform: uppercase;
	    transition: 0.2s box-shadow ease-in-out, 0.2s background-color ease-in-out, 0.2s border-color ease-in-out;
	    white-space: nowrap;
	}
</style>

<div style="text-align: center; font-family: Open Sans; padding-left: 20px; padding-right: 20px;">
	<div style="border-radius: 4px; box-shadow: 4px 4px 20px rgba(0, 0, 0, 0.1); background: #e0e0e0; padding-top: 50px; padding-bottom: 50px; padding-right: 5px; padding-left: 5px; margin: 0px;">
		<h1>Hello {{ $user->first_name }}</h1>,

		<p>
			Please click the button to reset your password.
		</p>
		<p>
			<a class="my-button" href="{{env('APP_URL')}}/reset/{{ $user->email }}/{{$code}}"
				style="
				text-decoration: none;
				border: none;
			    background: #02b3e4;
			    padding: 1.2em 4em;
			    border-radius: 4px !important;
			    box-shadow: 4px 4px 20px rgba(0, 0, 0, 0.1);
			    color: #fff;
			    cursor: pointer;
			    display: inline-block;
			    font-family: 'Open Sans', sans-serif;
			    font-size: 12px;
			    font-weight: 600;
			    -webkit-font-smoothing: antialiased;
			    letter-spacing: 0.165em;
			    max-width: 100%;
			    overflow: hidden;
			    text-align: center;
			    text-overflow: ellipsis;
			    text-transform: uppercase;
			    transition: 0.2s box-shadow ease-in-out, 0.2s background-color ease-in-out, 0.2s border-color ease-in-out;
			    white-space: nowrap;
				">
				Reset Password
			</a>

		</p>
		<p>
			or you could copy this link and paste in your browser
		</p>
		<p>
			{{env('APP_URL')}}/reset/{{ $user->email }}/{{$code}}
			{{-- {{ route('reset.pass'), [$user, $code] }} --}}
		</p>
	</div>
	<div style="margin: 0px; padding: 10px; text-align: center;">
		<p>Copyright &copy; <a href="http://conference.biocharng.com">Biochar Nigeria</a> {{ date('Y') }}</p>
	</div>
</div>