<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home');

//Authentication
Route::get('/Signin', 'AuthController@getSignin')->name('auth.signin');

Route::get('/Signup', 'AuthController@getSignup')->name('auth.signup');

Route::post('/Signin', 'AuthController@postSignin');

Route::post('/Signup', 'AuthController@postSignup');

Route::get('/signout', 'AuthController@getSignout')->name('auth.signout');

// Submit Paper
Route::get('/submitpaper', 'PaperController@getSubmit')->name('paper.submit')->middleware(['member']);

Route::post('/submitpaper', 'PaperController@postSubmit')->middleware(['member']);

/*
*****ADMIN PANEL********
*/
Route::get('/dashboard', 'AdminController@index')->name('dashboard')->middleware('isAdmin');
Route::get('/dashboard/papers', 'AdminController@getPapers')->name('dashboard.papers')->middleware('isAdmin');
Route::get('/dashboard/users', 'AdminController@getusers')->name('dashboard.users')->middleware('isAdmin');


// Demo
Route::get('/demo', 'DemoController@index')->name('demo');

// password reset
Route::get('/password/reset', 'PasswordResetController@index')->name('password.reset');
Route::post('/password/reset', 'PasswordResetController@sendReset')->name('post.reset');
Route::get('/reset/{mail}/{code}', 'PasswordResetController@resetPassword')->name('reset.pass');
Route::post('/reset/{mail}/{code}', 'PasswordResetController@postResetPassword');

// {{env('APP_URL')}}/reset/{{ $user->email }}/{{$code}}